def code_morze(value):
    '''
    please add your solution here or call your solution implemented in different function from here
    then change return value from 'False' to value that will be returned by your solution
    '''
    morze_dict =  {'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.', 'F': '..-.', 'G': '--.', 'H': '....', 'I': '..', 'J': '.---', 'K': '-.-', 'L': '.-..', 'M': '--', 'N': '-.', 'O': '---', 'P': '.--.', 'Q': '--.-', 'R': '.-.', 'S': '...', 'T': '-', 'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-', 'Y': '-.--', 'Z': '--..', '1': '.----', '2': '..---', '3': '...--', '4': '....-', '5': '.....', '6': '-....', '7': '--...', '8': '---..', '9': '----.', '0': '-----', ', ': '--..--', '.': '.-.-.-', '?': '..--..', '/': '-..-.', '-': '-....-', '(': '-.--.', ')': '-.--.-'}
    encoded_str = ''
    for ind, char in enumerate(value):
        char = char.upper()
        if char in morze_dict:
            encoded_str += morze_dict[char]
        if ind != len(value) - 1 and char != ' ':
            encoded_str += ' '
    return encoded_str



